import {shallowMount} from '@vue/test-utils';
import InputViewClassStyle from '@/components/InputViewClassStyle.vue';

describe('InputViewClassStyle.vue', () => {
    it('renders input field', () => {
        const wrapper = shallowMount(InputViewClassStyle, {});
        expect(wrapper.find('input').exists()).toBe(true);
    });
});
