import {ValidateGutscheinResponse} from '@/services/Responses/ValidateGutscheinResponse';
import any = jasmine.any;

enum HttpMethodName {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
}

export default class ApiService {

    public async validateGutschein(gutscheinCode: string): Promise<ValidateGutscheinResponse | void> {
        const response = await this.callApi('5d014ab43200002800f9db76', HttpMethodName.GET, null);
        return JSON.parse(await response.text());
    }

    private async callApi(path: string, httpMethod: HttpMethodName, payload: null | string): Promise<Response> {
        const initFetch: any = {
            method: httpMethod,
            cache: 'no-cache',
            credentials: 'same-origin',
            headers:
                {
                    'Content-Type': 'application/json',
                },
            redirect: 'follow',
            referrer: 'no-referrer',
        };

        if (httpMethod !== HttpMethodName.GET && payload !== null) {
            initFetch.body = payload;
        }

        return fetch(
            'http://www.mocky.io/v2/' + path,
            initFetch,
        );
    }
}
